<div class="container">
    
    <div class="row mt-5">
        <div class="col-md-2">
          <a class="btn btn-primary" data-toggle="collapse" href="#collapseForm" aria-expanded="false" aria-controls="collapseExample">
              Nova Conta
          </a>
        </div>
        <div class="col-md-2 offset-md-7 mt-3">
          <input type="month" id="month" name="month" value="<?= set_value('month') ?>">
        </div>
    </div>

    <?php echo form_error('parceiro', '<div class="alert-danger">', '</div>'); ?>
    <?php echo form_error('descricao', '<div class="alert-danger">', '</div>'); ?>
    <?php echo form_error('valor', '<div class="alert-danger">', '</div>'); ?>
    <?php echo form_error('mes', '<div class="alert-danger">', '</div>'); ?>
    <?php echo form_error('ano', '<div class="alert-danger">', '</div>'); ?>

    <div class="collapse" id="collapseForm">
        <div class="row">
            <div class="col-md-6 mx-auto border mt-5 pt-3 pb-3">
                <form method="POST" id="contas-form">
                    <input value="<?= set_value('parceiro') ?>" type="text" id="parceiro" name="parceiro" class="form-control" placeholder="Devedor/Credor"><br>
                    <input value="<?= set_value('descricao') ?>" type="text" id="descricao" name="descricao" class="form-control" placeholder="Descrição"><br>
                    <input value="<?= set_value('valor') ?>" type="number" id="valor" name="valor" class="form-control" placeholder="Valor"><br><br>
                    
                    <input type="hidden" name="id" id="conta_id">
                    <input type="hidden" name = "tipo" value="<?= $tipo ?>">

                    <div class="text-center text-md-left">
                        <a href="#" class="btn btn-primary" onclick = "document.getElementById('contas-form').submit();">Enviar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    
    <div class="row">
        <div class="col">
            <?= $lista ?>
        </div>
    </div>
</div>


<div
  class="modal fade"
  id="exampleModal"
  tabindex="-1"
  aria-labelledby="exampleModalLabel"
  aria-hidden="true"
>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Remoção de Contas</h5>
        <button
          type="button"
          class="btn-close"
          data-mdb-dismiss="modal"
          aria-label="Close"
        ></button>
      </div>
      <div class="modal-body">
        Deseja realmente eliminar esta conta? Esta ação é irreversível!
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-mdb-dismiss="modal">
          Cancelar
        </button>
        <button type="button" id="confirmDeleteBtn" class="btn btn-primary">Deletar</button>
      </div>
    </div>
  </div>
</div>


<script>
    row_id = 0;
    $(document).ready(function() {
      $('#month').change(loadMonth);
      $('.delete_btn').click(openModal);
      $('#confirmDeleteBtn').click(deleteRow);
      $('.edit_btn').click(exibeForm);
      $('.pay_btn').click(liquidaConta);
    });

    function loadMonth() {
      let [ano, mes] = this.value.split('-');
      var v = window.location.href.split('/');
      var url = v.slice(0, 7).join('/');
      url = url+"/"+mes+"/"+ano;
      window.location.href = url;
    }



    function exibeForm(){
      var row_id = this.id;
      var td = $("#"+row_id).parent().parent().parent().children();
      $('#parceiro').val($(td[0]).text());
      $('#descricao').val($(td[1]).text());
      $('#valor').val($(td[2]).text());
      $('#mes').val($(td[3]).text());
      $('#ano').val($(td[4]).text());
      $('#conta_id').val(row_id);
      $('#collapseForm').collapse('show');
    }

    function deleteRow() {
        var id = row_id;
        $.post(api('contas', 'delete_conta'), {id}, function(d, s, x){
            $('#exampleModal').modal('hide');
            $("#"+id).parent().parent().parent().remove();
        });
    }

    function openModal() {
        row_id = this.id;
        $('#exampleModal').modal();
    }

    function liquidaConta() {
      var id = this.id;
      $.post(api('contas', 'status_conta'), {id}, function(d, s, x){
        $('#'+id).toggleClass('text-muted green-text');
      });
    }

</script>